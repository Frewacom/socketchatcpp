#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QNetworkInterface>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QTcpServer *Server;
    QTcpSocket *Socket = nullptr;

private slots:
    void on_ServerStart_clicked();
    void readyRead();
    void newConnection();
    void disconnected();

    void on_Send_clicked();

private:
    Ui::MainWindow *ui;
    bool isServerRunning = false;
};

#endif // MAINWINDOW_H
