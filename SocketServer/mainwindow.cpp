#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Server = new QTcpServer(this);
    Server->connect(Server, SIGNAL(newConnection()),
                    this, SLOT(newConnection()));
    connect(ui->Input, SIGNAL(returnPressed()), this, SLOT(on_Send_clicked()));

    //QList<QHostAddress> addresses;

    QHostAddress localhost = QHostAddress::LocalHost;

    ui->HostAddressesCombo->addItem(localhost.toString());
    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
        {
            qDebug() << address.toString();
            ui->HostAddressesCombo->addItem(address.toString());
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newConnection()
{
    if (Socket == nullptr)
    {
        Socket = Server->nextPendingConnection();
        ui->Output->append("Client connected");
    }
    else
    {
        QTcpSocket *socket = Server->nextPendingConnection();
        socket->write("Connection denied");
        socket->flush();
        socket->close();
    }

    Socket->connect(Socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    Socket->connect(Socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
}

void MainWindow::readyRead()
{
    QByteArray byteArray = Socket->readAll();
    QString data = byteArray.data();
    QString str = "<span style='color:#1981ff;font-weight:bold;'>Client:</span> ";
    ui->Output->append(str + data);
}

void MainWindow::disconnected()
{
    Socket = nullptr;
    ui->Output->append("Client disconnected");
}

void MainWindow::on_ServerStart_clicked()
{
    if (!isServerRunning)
    {
        if (!ui->HostAddressesCombo->currentText().isEmpty())
        {
            QHostAddress address(ui->HostAddressesCombo->currentText());

            if (!Server->listen(address, 1337))
            {
                ui->Output->append("Could not start server");
            }
            else
            {
                ui->Output->append("Server started successfully");
                ui->Output->append("Running on IP: " + address.toString());
                isServerRunning = true;

                ui->ServerStart->setText("Stop server");
                ui->HostAddressesCombo->setEnabled(false);
            }
        }
        else
        {
            ui->Output->append("You didn't select a hostname in the dropdown");
        }
    }
    else
    {
        Server->close();
        Socket->deleteLater();
        Socket = nullptr;
        ui->ServerStart->setText("Start server");
        ui->Output->append("Server stopped");
        ui->HostAddressesCombo->setEnabled(true);
        isServerRunning = false;
    }
}

void MainWindow::on_Send_clicked()
{
    if (isServerRunning && Socket != nullptr)
    {
        QString input = ui->Input->text();

        if (!input.isEmpty())
        {
            ui->Input->clear();
            QString str = "<span style='color:red;font-weight:bold;'>Server:</span> ";
            ui->Output->append(str + input);

            Socket->write(input.toUtf8());
            Socket->flush();

            ui->Input->setFocus();
        }
    }
}
