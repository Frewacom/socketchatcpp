#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QTcpSocket *Socket;
    char Buffer[1024];

private slots:
    void on_ServerConnect_clicked();
    void connected();
    void disconnected();
    void readyRead();

    void on_Send_clicked();
    void socketError(QAbstractSocket::SocketError *error);

private:
    Ui::MainWindow *ui;
    bool isConnected = false;
};

#endif // MAINWINDOW_H
