#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->Input, SIGNAL(returnPressed()), this, SLOT(on_Send_clicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_ServerConnect_clicked()
{
    qDebug() << "Clicked";

    if (!isConnected)
    {
        QString host = ui->IPInput->text();
        QHostAddress addr(host);

        Socket = new QTcpSocket(this);
        Socket->connectToHost(addr, 1337);

        Socket->connect(Socket, SIGNAL(connected()), this, SLOT(connected()));
        Socket->connect(Socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
        Socket->connect(Socket, SIGNAL(error(QAbstractSocket::SocketError)),
                                       this, SLOT(socketError(QAbstractSocket::SocketError)));
        Socket->connect(Socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    }
    else
    {
        ui->Output->append("Disconnecting");
        ui->ServerConnect->setText("Connect");
        isConnected = false;
        Socket->close();
    }

}

void MainWindow::socketError(QAbstractSocket::SocketError *error)
{
    ui->Output->append("Socket error: ");
}

void MainWindow::connected()
{
    ui->Output->append("Connected to host");
    ui->ServerConnect->setText("Disconnect");
    ui->IPInput->setEnabled(false);
    isConnected = true;
}

void MainWindow::disconnected()
{
    Socket->deleteLater();
    Socket = nullptr;
    ui->Output->append("Disconnected from host");
    ui->IPInput->setEnabled(true);
}

void MainWindow::readyRead()
{
    QByteArray byteArray = Socket->readAll();
    QString data = byteArray.data();
    QString str = "<span style='color:red;font-weight:bold;'>Server:</span> ";
    ui->Output->append(str + data);
}

void MainWindow::on_Send_clicked()
{
    if (isConnected && Socket != nullptr)
    {
        QString input = ui->Input->text();

        if (!input.isEmpty())
        {
            ui->Input->clear();
            QString str = "<span style='color:#1981ff;font-weight:bold;'>You:</span> ";
            ui->Output->append(str + input);

            Socket->write(input.toUtf8());
            Socket->flush();

            ui->Input->setFocus();
        }
    }
}
