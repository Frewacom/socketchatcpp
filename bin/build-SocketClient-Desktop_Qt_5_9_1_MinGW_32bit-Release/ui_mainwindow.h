/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *IPInput;
    QPushButton *ServerConnect;
    QTextEdit *Output;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *Input;
    QPushButton *Send;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QLatin1String("*\n"
"{\n"
"	font-family: \"Segoe UI\";\n"
"	font-size: 12px;\n"
"}\n"
"\n"
"QWidget\n"
"{\n"
"	background-color: #4f4f4f;\n"
"}\n"
"\n"
"QComboBox\n"
"{\n"
"	height: 20px;\n"
"	background-color: white;\n"
"	color: black;\n"
"	padding: 5px 10px;\n"
"	border: none;\n"
"}\n"
"\n"
"QComboBox::drop-down \n"
"{\n"
"    border: 0px;\n"
"	color: black;\n"
"}\n"
"\n"
"QComboBox QAbstractItemView\n"
"{\n"
"	background-color: #a6a6a6;\n"
"	padding: 5px;\n"
"	selection-background-color: transparent;\n"
"	border: none;\n"
"	outline: 0px;\n"
"}\n"
"\n"
"QTextEdit\n"
"{\n"
"	background-color: #1b1b1b;\n"
"	border: none;\n"
"	color: white;\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"	height: 20px;\n"
"	background-color: #1b1b1b;\n"
"	border: none;\n"
"	color: white;\n"
"	padding: 5px 10px 5px 10px;\n"
"}\n"
"\n"
"QLineEdit\n"
"{\n"
"	height: 20px;\n"
"	background-color: white;\n"
"	border: none;\n"
"	color: black;\n"
"	padding: 5px 10px 5px 10px;\n"
"}"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        IPInput = new QLineEdit(centralWidget);
        IPInput->setObjectName(QStringLiteral("IPInput"));

        horizontalLayout_3->addWidget(IPInput);

        ServerConnect = new QPushButton(centralWidget);
        ServerConnect->setObjectName(QStringLiteral("ServerConnect"));

        horizontalLayout_3->addWidget(ServerConnect);


        verticalLayout->addLayout(horizontalLayout_3);

        Output = new QTextEdit(centralWidget);
        Output->setObjectName(QStringLiteral("Output"));
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI"));
        Output->setFont(font);
        Output->setReadOnly(true);

        verticalLayout->addWidget(Output);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        Input = new QLineEdit(centralWidget);
        Input->setObjectName(QStringLiteral("Input"));

        horizontalLayout_2->addWidget(Input);

        Send = new QPushButton(centralWidget);
        Send->setObjectName(QStringLiteral("Send"));
        Send->setAutoDefault(true);
        Send->setFlat(false);

        horizontalLayout_2->addWidget(Send);


        verticalLayout->addLayout(horizontalLayout_2);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        Send->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        IPInput->setPlaceholderText(QApplication::translate("MainWindow", "Server hostname (on network)", Q_NULLPTR));
        ServerConnect->setText(QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        Input->setPlaceholderText(QApplication::translate("MainWindow", "Type message..", Q_NULLPTR));
        Send->setText(QApplication::translate("MainWindow", "Send", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
